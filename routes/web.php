<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ContactsController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\LocalizationController;
use App\Http\Controllers\PersonalController;
use App\Http\Controllers\SettingsController;
use Illuminate\Support\Facades\Route;

Route::controller(AuthController::class)->group(function () {
    Route::get('/', 'index')->name('login');
    Route::post('login', 'postLogin')->name('login.post');

    Route::get('lang/{locale}', [LocalizationController::class, 'index']);

    Route::middleware('auth')->group(function () {
        Route::post('logout', 'logout')->name('logout');

        Route::resource('contacts', ContactsController::class);
        Route::resource('departments', DepartmentController::class);
        Route::resource('settings', SettingsController::class);
        Route::post('settings/add-position', [SettingsController::class, 'addPosition'])->name('add-position');

        Route::post('personal/download/{id}', [PersonalController::class, 'download']);
        Route::get('personal/work-time', [PersonalController::class, 'workTime'])->name('work-time');
        Route::post('personal/add-time', [PersonalController::class, 'addTime'])->name('add-time');
        Route::resource('personal', PersonalController::class);

        Route::get('country-list', [ContactsController::class, 'countryList']);
        Route::get('positions-list', [ContactsController::class, 'positionsList']);
        Route::get('departments-list', [ContactsController::class, 'departmentsList']);
    });
});


