$(function() {
    let token = $('meta[name="csrf-token"]').attr('content');
    $.ajaxSetup({
        headers: {'x-csrf-token': token}
    });

    $('#submitEmployee').on('click', function (e) {
        e.preventDefault();

        let formData = $('#newEmployeeForm').serializeArray();
        let addressItems = ['address_1', 'address_2', 'city', 'zip_code'];
        let userData = {};
        let addressData = {};

        addressData['country_id'] = $( '#inputCountry').val();
        userData['position_id'] = $( '#inputPosition').val();
        userData['department_id'] = $( '#inputDepartment').val();
        $.each(formData, function() {
            if (this.name === '_token') {
                return true;
            }

            if ($.inArray(this.name, addressItems) !== -1) {
                addressData[this.name] = this.value;
            } else {
                userData[this.name] = this.value;
            }
        });

        $.ajax({
            url: "settings",
            type: "POST",
            data: {
                'userData': userData,
                'addressData': addressData
            },
            success: function () {
                alert('Pavyko');
            },
            error: function () {
                alert('Sveikinu, ką tik sugadinai sistemą. Tikiuosi džiaugiesi.');
            }
        });
    });

    $('#submitPosition').on('click', function (e) {
        e.preventDefault();

        let formData = $('#addPositionForm').serializeArray();

        $.ajax({
            url: "settings/add-position",
            type: "POST",
            data: formData,
            success: function () {
                alert('Pavyko');
            },
            error: function () {
                alert('Sveikinu, ką tik sugadinai sistemą. Tikiuosi džiaugiesi.');
            }
        });
    });

    $('#submitEditPersonal').on('click', function (e) {
        e.preventDefault();

        let form = $('#editPersonalForm');
        let formData = form.serializeArray();
        let addressItems = ['address_1', 'address_2', 'city', 'zip_code'];
        let userData = {};
        let addressData = {};

        $.each(formData, function() {
            if (this.name === '_token') {
                return true;
            }

            if ($.inArray(this.name, addressItems) !== -1) {
                addressData[this.name] = this.value;
            } else {
                userData[this.name] = this.value;
            }
        });

        $.ajax({
            url: "settings/" + form.data('id'),
            type: "PATCH",
            data: {
                'userData': userData,
                'addressData': addressData
            },
            success: function () {
                alert('Pavyko');
            },
            error: function () {
                alert('Sveikinu, ką tik sugadinai sistemą. Tikiuosi džiaugiesi.');
            }
        });
    });

    $('#add-employee').click(function() {
        $('#employee-card').slideToggle();
    });

    $('#add-position').click(function() {
        $('.position-card').slideToggle();
    });

    $('#edit-personal').click(function() {
        $('#personal-card').slideToggle();
    });
});
