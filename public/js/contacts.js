function format(d) {
    return '<table class="child-table" style="display: table; margin-left: 41px; width: 90%;">' +
        '<tr class="children-row">' +
        '<td>Miestas: ' + d.address.city + '</td>' +
        '<td>Adresas: ' + d.address.address_1 + '</td>' +
        '<td>Zip kodas: ' + d.address.zip_code + '</td>' +
        '</tr>' +
        '</table>';
}

$(function() {
    let token = $('meta[name="csrf-token"]').attr('content');
    $.ajaxSetup({
        headers: {'x-csrf-token': token}
    });

    let cTable = $('#datatable-Contact').DataTable({
        processing: true,
        serverSide: true,
        responsive: {
            details: {
                type: 'column',
                target: -1
            }
        },
        ajax: {
            url: "contacts",
            data: function (d) {
                d.countryFilter = $('#filterCountry').val();
                d.continentFilter = $('#filterContinent').val();
                d.positionFilter = $('#filterPosition').val();
            }
        },
        columns: [
            {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false, className: "align-box"},
            {data: 'workTimeColor', name: 'workTimeColor', orderable: false, searchable: false, className: "align-box"},
            {data: {name: 'name', id: 'id'}, name: 'name', render: function(data) {
                return '<a class="xedit" data-pk="' + data.id + '" href="#" data-name="name">' + data.name + '</a>';}
            },
            {data: {surname: 'surname', id: 'id'}, name: 'surname', render: function(data) {
                    return '<a class="xedit" data-pk="' + data.id + '" href="#" data-name="surname">' + data.surname + '</a>';}
            },
            {data: {email: 'email', id: 'id'}, name: 'email', render: function(data) {
                return '<a class="xedit" data-pk="' + data.id + '" href="#" data-name="email">' + data.email + '</a>';}
            },
            {data: {country: 'country_id', id: 'id'}, name: 'country_id', render: function(data) {
                    return '<a class="xselect" data-type="select" data-source="country-list" data-pk="' + data.id + '" href="#" data-name="country_id">' + (data.address?.country?.name ?? '-') + '</a>';}
            },
            {data: {position: 'position_id', id: 'id'}, name: 'position_id', render: function(data) {
                    return '<a class="xselect" data-type="select" data-source="positions-list" data-pk="' + data.id + '" href="#" data-name="position_id">' + (data.position?.name ?? '-') + '</a>';}
            },
            {data: {position: 'department_id', id: 'id'}, name: 'department_id', render: function(data) {
                    return '<a class="xselect" data-type="select" data-source="department-list" data-pk="' + data.id + '" href="#" data-name="department_id">' + (data.department?.name ?? '-') + '</a>';}
            },
            {data: {phone: 'phone', id: 'id'}, name: 'phone', render: function(data) {
                return '<a class="xedit" data-pk="' + data.id + '" href="#" data-name="phone">' + data.phone + '</a>';}
            },
            {data: 'created_at', name: 'created_at'},
            {className: 'details-control', orderable: false, data: null, defaultContent: '<span class="arrow2"></span>'}
        ],
        columnDefs: [{
            responsivePriority: 1,
            targets: -1
        }],
        paging: false,
        info: false,
        order: [],
        select: {
            style: 'multi+shift',
            selector: 'td:first-child'
        },
        language: {
            'processing': '<i class=\'fa fa-refresh fa-spin\' style="font-size: 30px"></i>',
            'zeroRecords': 'Įrašų nerasta'
        },
        drawCallback: function() {
            //console.log(settings._iRecordsTotal); //gal kazkada prireiks - bendras lentelėje esančių eilučių skaičius
            let xedit = $('.xedit');
            let xselect = $('.xselect');
            xedit.add(xselect).on('init', function (e, edt) {
                edt.options.url = 'contacts/' + edt.options.pk;
            });

            xedit.editable({
                mode: 'inline',
                emptytext: 'Tuščia',
                showbuttons: false,
                inputclass: 'input-small',
                savenochange: true,
                ajaxOptions: {
                    type: 'PATCH'
                },
                error: function(response) {
                    let error = `<i class="fa-solid fa-circle-exclamation exclamation-icon"></i>
                        <span style="font-weight: 600; letter-spacing: 0.4px;">`+response.responseJSON.message+`</span>`;

                    $('#restriction').html('').append(error).fadeIn(600).delay(3000).fadeOut(600);
                },
            });

            xselect.editable({
                showbuttons: false,
                savenochange: true,
                onblur: 'ignore',
                ajaxOptions: {type: 'PATCH'},
                error: function(response) {
                    let error = `<i class="fa-solid fa-circle-exclamation exclamation-icon"></i>
                        <span style="font-weight: 600; letter-spacing: 0.4px;">`+response.responseJSON.message+`</span>`;

                    $('#restriction').html('').append(error).fadeIn(600).delay(3000).fadeOut(600);
                },
            });
        }
    });

    //paspaudus ant rodykles atsidaro tiek responsive, tiek child row funkcijos
    cTable.on('responsive-display', function (e, datatable, row) {
        $('.dtr-details').append(format(row.data()));
    });

    $('#datatable-Contact tbody').on('click', 'td.details-control', function() {
        if (!cTable.responsive.hasHidden()) {
            let tr = $(this).closest('tr');
            let row = cTable.row(tr);

            if (row.child.isShown()) {
                row.child.hide();
                tr.removeClass('shown');
            } else {
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        }
    });

    //padaro atsiuntimo mygtuka pasirinktoje vietoje
    $("#download-button").on("click", function() {
        cTable.button('.buttons-excel').trigger();
    });

    //pelyte uzslinkus ant vaikines eilutes, uzsidega ir tevine eilute
    $("#datatable-Contact tbody").on({
        mouseenter: function() {
            $(this).prev().addClass("select-upper double-background");
            $(this).prev().find(">:first-child").addClass("round-border-left");
            $(this).prev().find(">:last-child").addClass("round-border-right");
        },
        mouseleave: function() {
            $(this).prev().removeClass("select-upper double-background");
            $(this).prev().find(">:first-child").removeClass("round-border-left");
            $(this).prev().find(">:last-child").removeClass("round-border-right");
        }
    }, 'tr:not(.odd, .even)');

    $('.search').keyup(function() {
        cTable.search($(this).val()).draw();
    });

    //abecele
    for (let i = 0; i < 26; i++) {
        let char = String.fromCharCode(65 + i);
        $("#sLetters").append("<div class='letter'>" + char + "</div>");
    }

    $('.letter').click(function() {
        cTable.column(1).search('^' + $(this).text(), true, false).draw();
        if ($(this).hasClass('clear-selection')) {
            cTable.column(1).search('', true, false).draw();
        }
    });

    $('.nLetter').click(function() {
        cTable.column(1).search('^' + '0|1|2|3|4|5|6|7|8|9', true, false).draw();
    });

    //pasirinkti viska
    $("#selectAll").click(function() {
        if ($(this).is(":checked")) {
            cTable.rows().select();
            $('input:checkbox').not(this).prop('checked', this.checked);
        } else {
            cTable.rows().deselect();
            $('input:checkbox').not(this).prop('checked', this.checked);
        }
    });

    $('#submitDepartment').on('click', function (e) {
        e.preventDefault();

        let departmentInput = $('#addDepartmentForm').serializeArray();
        let department = {};

        department['country_id'] = $( "#countryPick" ).val();
        $.each(departmentInput, function() {
            if (this.name === '_token') {
                return true;
            }

            department[this.name] = this.value;
        });

        $.ajax({
            url: "departments",
            type: "POST",
            headers: {'X-CSRF-TOKEN': token},
            data: {
                data: department
            },
            success: function () {
                $('#addNewModal').modal('toggle');
                cTable.ajax.reload();
            },
            error: function () {
                alert('Sveikinu, ką tik sugadinai sistemą. Tikiuosi džiaugiesi.');
            }
        });
    });

    $('#filterCountry').add('#filterContinent').add('#filterPosition').change(function () {
        cTable.draw();
    });

    //Paspaudus ant pašalinimo mygtuko, siunčia užklausą
    $('#remove-button').on('click', function (e) {
        e.preventDefault();

        let allSelected = $('input:checkbox:checked');
        let selectedIds = '';

        $(allSelected).each(function() {
            selectedIds += this.id + ",";
        });

        $.ajax({
            url: "contacts/" + selectedIds.slice(0,-1),
            type: "DELETE",
            headers: {'X-CSRF-TOKEN': token},
            success: function () {
                cTable.ajax.reload();
                $('#remove-button').fadeOut('fast');
            },
            error: function (response) {
                let error = `<i class="fa-solid fa-circle-exclamation exclamation-icon"></i>
                        <span style="font-weight: 600; letter-spacing: 0.4px;">`+response.responseJSON.message+`</span>`;

                $('#restriction').html('').append(error).fadeIn(600).delay(3000).fadeOut(600);
            }
        });
    });
})

$(document).ready(function() {
    $('#openFilter').click(function() {
        $('#filterForm').slideToggle();
    });

    $(window).on('load', function() {
        $("#datatable-Contact tr").hover(
            function() {
                $(this).addClass("select-upper");
            },
            function() {
                $(this).removeClass("select-upper");
            }
        );
    });

    //rodyklele lenteles ispletimui
    $(document).on('click', '.details-control', function() {
        $(this).find('.arrow2').toggleClass("arrow-rotate");
        $(this).toggleClass('double-select');
        $(this).parent().children(':first-child').toggleClass('double-select');
    });

    $('#newComment').on('click', function() {
        let fieldHTML =
            `<div class="mb-3">
                <textarea id="commentArea" class="form-control" rows="3" name="comment" aria-label="Komentaras"></textarea>
            </div>`;

        $(fieldHTML).insertBefore('.addComment');
        $('.addComment').hide();
    });

    //patikrinama ar yra pažymėtas bent vienas checkbox laukelis
    $(document).on('click', '.checkbox_check', function() {
        let numberOfChecked = $('input:checkbox:checked').length;

        if (numberOfChecked > 0) {
            $('#remove-button').fadeIn('fast');
        } else {
            $('#remove-button').fadeOut('fast');
        }
    });
})
