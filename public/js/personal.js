$(function() {
    let token = $('meta[name="csrf-token"]').attr('content');
    $.ajaxSetup({
        headers: {'x-csrf-token': token}
    });

    let pfTable = $('#datatable-PersonalFiles').DataTable({
        processing: true,
        serverSide: true,
        responsive: {
            details: {
                type: 'column',
                target: -1
            }
        },
        ajax: "personal",
        columns: [
            {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false, className: "align-box"},
            {data: {name: 'title', id: 'id'}, name: 'title', render: function(data) {
                return '<a class="xedit" data-pk="' + data.id + '" href="#" data-name="title">' + data.title + '</a>';}
            },
            {data: {email: 'details', id: 'id'}, name: 'details', render: function(data) {
                return '<a class="xedit" data-pk="' + data.id + '" href="#" data-name="details">' + (data.details ?? '-') + '</a>';}
            },
            {data: 'created_at', name: 'created_at', render: function(data) {
                    return data.substr(0, 10);}
            },
            {data: {email: 'download', id: 'id'}, name: 'download', orderable: false, render: function(data) {
                    return '<a class="download" data-pk="' + data.id + '" href="#" data-name="download" data-title="'+data.title+'">' +
                                '<i class="fa-solid fa-file-arrow-down"></i>' +
                           '</a>';}
            },
            {className: 'details-control', orderable: false, data: null, defaultContent: '<span class="arrow2"></span>'}
        ],
        columnDefs: [
            {responsivePriority: 1, targets: [-1, 4]},
            {responsivePriority: 2, targets: 3},
        ],
        paging: false,
        info: false,
        order: [],
        select: {
            style: 'multi+shift',
            selector: 'td:first-child'
        },
        language: {
            'processing': '<i class=\'fa fa-refresh fa-spin\' style="font-size: 30px"></i>',
            'zeroRecords': 'Failų nerasta'
        },
        drawCallback: function() {
            //console.log(settings._iRecordsTotal); //gal kazkada prireiks - bendras lentelėje esančių eilučių skaičius
            let xedit = $('.xedit');
            xedit.on('init', function (e, edt) {
                edt.options.url = 'personal/' + edt.options.pk;
            });

            xedit.editable({
                mode: 'inline',
                emptytext: 'Tuščia',
                showbuttons: false,
                inputclass: 'input-small',
                savenochange: true,
                ajaxOptions: {
                    type: 'PATCH'
                },
                error: function(response) {
                    let error = `<i class="fa-solid fa-circle-exclamation exclamation-icon"></i>
                        <span style="font-weight: 600; letter-spacing: 0.4px;">`+response.responseJSON.message+`</span>`;

                    $('#restriction').html('').append(error).fadeIn(600).delay(3000).fadeOut(600);
                },
            });

            $('.download').on('click', function (e) {
                e.preventDefault();

                let fileId = $(this).data('pk');
                let title = $(this).data('title');

                $.ajax({
                    url: "personal/download/" + fileId,
                    type: "POST",
                    headers: {'X-CSRF-TOKEN': token},
                    xhrFields: {
                        responseType: 'blob'
                    },
                    success: function (response) {
                        let link = document.createElement('a');
                        link.href = window.URL.createObjectURL(new Blob([response]));
                        link.download = title + '.docx';
                        link.click();
                    },
                    error: function () {
                        alert('nepavyko');
                    }
                });
            });
        }
    });

    let wtTable = $('#datatable-WorkTime').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: 'personal/work-time',
            type: 'GET'
        },
        columns: [
            {data: 'time_from', name: 'time_from', render: function(data) {
                    return data.substr(0, 10);}
            },
            {data: {time_from: 'time_from', id: 'id'}, name: 'time_from', render: function(data) {
                    return '<a class="xedit" data-pk="' + data.id + '" href="#" data-name="time_from">' + data.time_from.substr(11, 5) + '</a>';}
            },
            {data: {time_from: 'time_till', id: 'id'}, name: 'time_till', render: function(data) {
                    return '<a class="xedit" data-pk="' + data.id + '" href="#" data-name="time_till">' + data.time_till.substr(11, 5) + '</a>';}
            },
        ],
        paging: false,
        info: false,
        order: [],
        select: {
            style: 'multi+shift',
            selector: 'td:first-child'
        },
        language: {
            'processing': '<i class=\'fa fa-refresh fa-spin\' style="font-size: 30px"></i>',
            'zeroRecords': 'Įrašų nerasta'
        },
        drawCallback: function() {
            //console.log(settings._iRecordsTotal); //gal kazkada prireiks - bendras lentelėje esančių eilučių skaičius
            let xedit = $('.xedit');
            xedit.on('init', function (e, edt) {
                edt.options.url = 'personal/' + edt.options.pk;
            });

            xedit.editable({
                mode: 'inline',
                emptytext: 'Tuščia',
                showbuttons: false,
                inputclass: 'input-small',
                savenochange: true,
                ajaxOptions: {
                    type: 'PATCH'
                },
                error: function(response) {
                    let error = `<i class="fa-solid fa-circle-exclamation exclamation-icon"></i>
                        <span style="font-weight: 600; letter-spacing: 0.4px;">`+response.responseJSON.message+`</span>`;

                    $('#restriction').html('').append(error).fadeIn(600).delay(3000).fadeOut(600);
                },
            });
        }
    });
    //paspaudus ant rodykles atsidaro tiek responsive, tiek child row funkcijos
    pfTable.on('responsive-display', function (e, datatable, row) {
        $('.dtr-details').append(format(row.data()));
    });

    $('#datatable-PersonalFiles tbody').on('click', 'td.details-control', function() {
        if (!pfTable.responsive.hasHidden()) {
            let tr = $(this).closest('tr');
            let row = pfTable.row(tr);

            if (row.child.isShown()) {
                row.child.hide();
                tr.removeClass('shown');
            } else {
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        }
    });

    //pelyte uzslinkus ant vaikines eilutes, uzsidega ir tevine eilute
    $("#datatable-PersonalFiles tbody").on({
        mouseenter: function() {
            $(this).prev().addClass("select-upper double-background");
            $(this).prev().find(">:first-child").addClass("round-border-left");
            $(this).prev().find(">:last-child").addClass("round-border-right");
        },
        mouseleave: function() {
            $(this).prev().removeClass("select-upper double-background");
            $(this).prev().find(">:first-child").removeClass("round-border-left");
            $(this).prev().find(">:last-child").removeClass("round-border-right");
        }
    }, 'tr:not(.odd, .even)');

    //pasirinkti viska
    $("#selectAll").click(function() {
        if ($(this).is(":checked")) {
            pfTable.rows().select();
            $('input:checkbox').not(this).prop('checked', this.checked);
        } else {
            pfTable.rows().deselect();
            $('input:checkbox').not(this).prop('checked', this.checked);
        }
    });

    $('#submitFile').on('click', function (e) {
        e.preventDefault();

        let input = $('#addFileForm').serializeArray();
        let file = $('#formFile').prop('files')[0];
        let fileData = new FormData();

        fileData.append('file', file);

        $.each(input, function() {
            if (this.name === '_token') {
                return true;
            }

            fileData.append(this.name, this.value);
        });

        $.ajax({
            url: "personal",
            type: "POST",
            headers: {'X-CSRF-TOKEN': token},
            cache: false,
            processData: false,
            contentType: false,
            data: fileData,
            success: function () {
                pfTable.ajax.reload();
            },
            error: function (response) {
                let error = response.responseJSON.message;
                $('.error-message').html('').append(error).fadeIn(600).delay(3000).fadeOut(600);
            }
        });
    });

    $('#addTime').on('click', function (e) {
        e.preventDefault();

        let timeInput = $('#addWorkTimeForm').serializeArray();
        let time = {};

        $.each(timeInput, function() {
            if (this.name === '_token') {
                return true;
            }

            time[this.name] = this.value;
        });

        $.ajax({
            url: "personal/add-time",
            type: "POST",
            headers: {'X-CSRF-TOKEN': token},
            data: time,
            success: function () {
                wtTable.ajax.reload();
            },
            error: function (response) {
                let error = response.responseJSON.message;
                $('.error-message').html('').append(error).fadeIn(600).delay(3000).fadeOut(600);
            }
        });
    });

    //Paspaudus ant pašalinimo mygtuko, siunčia užklausą
    $('#remove-button').on('click', function (e) {
        e.preventDefault();

        let allSelected = $('input:checkbox:checked');
        let selectedIds = '';

        $(allSelected).each(function() {
            selectedIds += this.id + ",";
        });

        $.ajax({
            url: "personal/" + selectedIds.slice(0,-1),
            type: "DELETE",
            headers: {'X-CSRF-TOKEN': token},
            success: function () {
                pfTable.ajax.reload();
                $('#remove-button').fadeOut('fast');
            },
            error: function () {
                return 'Kažkas nepavyko';
            }
        });
    });
})

$(document).ready(function() {
    $('#addFile').click(function() {
        $('#fileToggle').slideToggle();
    });

    $('#addTime').click(function() {
        $('#timeToggle').slideToggle();
    });

    $(window).on('load', function() {
        $("#datatable-PersonalFiles tr").hover(
            function() {
                $(this).addClass("select-upper");
            },
            function() {
                $(this).removeClass("select-upper");
            }
        );
    });

    //rodyklele lenteles ispletimui
    $(document).on('click', '.details-control', function() {
        $(this).find('.arrow2').toggleClass("arrow-rotate");
        $(this).toggleClass('double-select');
        $(this).parent().children(':first-child').toggleClass('double-select');
    });

    new tempusDominus.TempusDominus(document.getElementById('date-from'), {
        display: {
            components: {
                useTwentyfourHour: true
            }
        }
    });
    new tempusDominus.TempusDominus(document.getElementById('date-till'), {
        display: {
            components: {
                useTwentyfourHour: true
            }
        }
    });

    //patikrinama ar yra pažymėtas bent vienas checkbox laukelis
    $(document).on('click', '.checkbox_check', function() {
        let numberOfChecked = $('input:checkbox:checked').length;

        if (numberOfChecked > 0) {
            $('#remove-button').fadeIn('fast');
        } else {
            $('#remove-button').fadeOut('fast');
        }
    });
})
