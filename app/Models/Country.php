<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Country extends Model
{
    use HasFactory;

    public $table = 'countries';

    protected $guarded = [];

    public function department(): HasOne
    {
        return $this->hasOne(Department::class, 'country_id', 'id');
    }

    public function address(): HasMany
    {
        return $this->hasMany(Address::class, 'country_id', 'id');
    }

    public function continent(): BelongsTo
    {
        return $this->belongsTo(Continent::class, 'continent_id', 'id');
    }
}
