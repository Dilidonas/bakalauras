<?php

namespace App\Http\Requests;

use Illuminate\Auth\Events\Lockout;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class EmployeeRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'userData.name' => 'string|min:3|max:50',
            'userData.surname' => 'string|min:3|max:50',
            'userData.department_id' => 'integer',
            'userData.position_id' => 'integer',
            'userData.email' => 'string|email',
            'userData.phone' => 'string',
            'addressData.address_1' => 'string',
            'addressData.address_2' => 'nullable|string',
            'addressData.city' => 'string',
            'addressData.zip_code' => 'string',
            'addressData.country_id' => 'integer',
            'userData.password' => 'between:6,255|confirmed',
            '*' => 'required'
        ];
    }
}
