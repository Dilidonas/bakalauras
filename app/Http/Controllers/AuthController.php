<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
            return redirect()->intended('departments');
        }

        return view('auth.login');
    }

    public function postLogin(LoginRequest $request)
    {
        $credentials = Arr::except($request->validationData(), '_token');
        if (Auth::attempt($credentials)) {
            return redirect()->intended('departments')->withSuccess(__('Logged in successfully!'));
        }

        return redirect('/')->withSuccess(__('Invalid credentials. Please try again'));
    }

    public function logout() {
        Session::flush();
        Auth::logout();

        return redirect('/');
    }
}
