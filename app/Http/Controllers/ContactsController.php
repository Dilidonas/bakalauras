<?php

namespace App\Http\Controllers;

use App\Models\Continent;
use App\Models\Country;
use App\Models\Department;
use App\Models\Position;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Yajra\DataTables\DataTables;

class ContactsController extends Controller
{
    public function index(Request $request)
    {
        $countries = Country::has('address.user')->get();
        $continents = Continent::has('countries.address.user')->get();
        $positions = Position::has('users')->get();

        if ($request->ajax()) {
            $users = User::with(['position', 'address', 'address.country', 'department', 'workTime' => function ($query) {
                $query->whereDate('time_till', Carbon::today())
                    ->where('time_till', '>', now());
            }])->when($request->get('countryFilter') > 0, function ($query) use ($request) {
                    return $query->whereRelation('country', 'id', '=', $request->get('countryFilter'));
                })
                ->when($request->get('continentFilter') > 0, function ($query) use ($request) {
                    return $query->whereRelation('country.continent', 'id', '=', $request->get('continentFilter'));
                })
                ->when($request->get('positionFilter') > 0, function ($query) use ($request) {
                    return $query->whereRelation('position', 'id', '=', $request->get('positionFilter'));
                })
                ->get();

            return DataTables::of($users)
                ->addIndexColumn()
                ->addColumn('checkbox', function ($data) {
                    return '<label class="checkboxColor"><input type="checkbox" id="' . $data->id . '" name="someCheckbox" class="checkbox_check"/></label>';
                })
                ->addColumn('workTimeColor', function ($user) {
                    $color = !$user->workTime->isEmpty() ? 'activeColor' : 'inactiveColor';

                    return '<div id="workTimeColor" class="' . $color . '"></div>';
                })
                ->rawColumns(['action', 'checkbox', 'workTimeColor'])
                ->make(true);
        }

        return view('sections.contacts', compact('countries', 'continents', 'positions'));
    }

    public function update(Request $request)
    {
        if (!auth()->user()->can('edit table')) {
            return response()->json([
                'message' => 'Veiksmas uždraustas'
            ], 401);
        }

        if ($request->name === 'country_id') {
            $address = User::find($request->pk)->address;

            return $address->update(['country_id' => $request->value]);
        }

        if ($request->name === 'position_id') {
            return User::find($request->pk)
                ->update(['position_id' => $request->value]);
        }

        $user = User::find($request->pk);

        return $user->update([$request->name => $request->value]);
    }

    public function countryList(): Collection|array
    {
        $countries = Country::all();

        foreach ($countries as $country) {
            $country->value = $country->id;
            $country->text = $country->name;
        }

        return $countries;
    }

    public function positionsList(): Collection|array
    {
        $positions = Position::all();

        foreach ($positions as $position) {
            $position->value = $position->id;
            $position->text = $position->name;
        }

        return $positions;
    }

    public function departmentsList(): Collection|array
    {
        $departments = Department::all();

        foreach ($departments as $department) {
            $department->value = $department->id;
            $department->text = $department->name;
        }

        return $departments;
    }

    public function destroy(string $rowIds)
    {
        if (!auth()->user()->can('edit table')) {
            return response()->json([
                'message' => 'Veiksmas uždraustas'
            ], 401);
        }

        $ids = explode(",", $rowIds);

        return User::whereIn('id', $ids)->delete();
    }
}
