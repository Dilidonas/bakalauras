<?php

namespace App\Http\Controllers;

use App\Http\Requests\FileRequest;
use App\Http\Requests\TimeRequest;
use App\Models\Country;
use App\Models\File;
use App\Models\WorkTime;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

class PersonalController extends Controller
{
    public function index(Request $request)
    {
        $countries = Country::all();

        if ($request->ajax()) {
            $data = File::with('user')->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('checkbox', function ($data) {
                    return '<label class="checkboxColor"><input type="checkbox" id="' . $data->id . '" name="someCheckbox" class="checkbox_check"/></label>';
                })
                ->rawColumns(['action', 'checkbox'])
                ->make(true);
        }

        return view('sections.personal', compact('countries'));
    }

    public function store(FileRequest $request)
    {
        $data = $request->validationData();

        $duplicateName = File::whereTitle($data['title'])->first();
        if ($duplicateName) {
            return response()->json([
                'message' => 'Failas su tokiu pavadinimu jau yra pridėtas'
            ], 400);
        }

        $path = Storage::putFile('documents', $data['file']);

        File::create([
            'user_id' => Auth()->id(),
            'title' => $data['title'],
            'details' => $data['details'],
            'path' => $path
        ]);

        return back();
    }

    public function update(Request $request)
    {
        if ($request->name === 'time_from' || $request->name === 'time_till') {
            $workingTime = WorkTime::find($request->pk);
            $date = Carbon::parse($workingTime->time_from)->format('Y-m-d');

            return $workingTime->update([$request->name => $date . ' ' . $request->value]);
        }

        $user = File::find($request->pk);

        return $user->update([$request->name => $request->value]);
    }

    public function download(int $fileId)
    {
        $file = File::where('user_id', Auth()->id())
            ->whereId($fileId)->first();

        if (!$file) {
            return null;
        }

        $link = storage_path('app/' . $file->path);
        if (\Illuminate\Support\Facades\File::exists($link)) {
            return Storage::download($file->path);
        }

        return back();
    }

    public function workTime()
    {
        $data = WorkTime::where('user_id', Auth()->id())->get();

        return DataTables::of($data)
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    public function addTime(TimeRequest $request)
    {
        $data = $request->validationData();

        WorkTime::create([
            'user_id' => Auth()->id(),
            'time_from' => $data['date_from'],
            'time_till' => $data['date_till']
        ]);

        return back();
    }

    public function destroy(string $rowIds)
    {
        $ids = explode(",", $rowIds);

        return File::whereIn('id', $ids)->delete();
    }
}
