<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeRequest;
use App\Http\Requests\PersonalEditRequest;
use App\Models\Address;
use App\Models\Country;
use App\Models\Department;
use App\Models\Position;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SettingsController extends Controller
{
    public function index()
    {
        $countries = Country::all();
        $departments = Department::all();
        $positions = Position::all();
        $personal = User::with('address')->find(Auth()->id());

        return view('sections.settings', compact('countries', 'departments', 'positions', 'personal'));
    }

    public function store(EmployeeRequest $request)
    {
        $data = $request->validationData();
        $data['userData']['password'] = Hash::make($data['userData']['password']);

        $address = Address::create($data['addressData']);

        $data['userData']['address_id'] = $address->id;
        $user = User::create($data['userData']);

        $user->assignRole('employee');

        return back();
    }

    public function update(PersonalEditRequest $request, int $id)
    {
        $data = $request->validationData();

        $user = User::find($id);
        $user->address()->update($data['addressData']);

        return $user->update($data['userData']);
    }

    public function addPosition(Request $request)
    {
        Position::create(['name' => $request->name]);

        return back();
    }
}
