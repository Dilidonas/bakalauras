<?php

namespace App\Http\Controllers;

use App\Models\Department;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Yajra\DataTables\DataTables;

class LocalizationController extends Controller
{
    public function index($locale): RedirectResponse
    {
        App::setLocale($locale);
        session()->put('locale', $locale);

        return back();
    }
}
