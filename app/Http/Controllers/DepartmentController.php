<?php

namespace App\Http\Controllers;

use App\Http\Requests\DepartmentRequest;
use App\Models\Continent;
use App\Models\Country;
use App\Models\Department;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class DepartmentController extends Controller
{
    public function index(Request $request)
    {
        $countries = Country::has('department')->get();
        $continents = Continent::has('countries.department')->get();

        if ($request->ajax()) {
            $data = Department::with('country', 'users')
                ->when($request->get('countryFilter') > 0, function ($query) use ($request) {
                    return $query->whereRelation('country', 'id', '=', $request->get('countryFilter'));
                })
                ->when($request->get('continentFilter') > 0, function ($query) use ($request) {
                    return $query->whereRelation('country.continent', 'id', '=', $request->get('continentFilter'));
                })
                ->when($request->get('commentFilter') === 'true', function ($query) {
                    return $query->whereNotNull('comment');
                })
                ->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('checkbox', function ($data) {
                    return '<label class="checkboxColor"><input type="checkbox" id="'. $data->id .'" name="someCheckbox" class="checkbox_check"/></label>';
                })
                ->rawColumns(['action', 'checkbox'])
                ->make(true);
        }

        return view('sections.departments', compact('countries', 'continents'));
    }

    public function store(DepartmentRequest $request): RedirectResponse
    {
        $data = $request->validationData();
        Department::create($data['data']);

        return back();
    }

    public function update(Request $request)
    {
        $department = Department::find($request->pk);

        return $department->update([$request->name => $request->value]);
    }

    public function destroy(string $rowIds)
    {
        if (!auth()->user()->can('edit table')) {
            return response()->json([
                'message' => 'Veiksmas uždraustas'
            ], 401);
        }

        $ids = explode(",", $rowIds);

        return Department::whereIn('id', $ids)->delete();
    }
}
