<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login' => 'Prisijungti',
    'login_title' => 'Prisijungimas',
    'email' => 'El. paštas',
    'password' => 'Slaptažodis',
    'forgot' => 'Pamiršote slaptažodį?',
    'remember' => 'Prisiminti'

];
