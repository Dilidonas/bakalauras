<?php

return [
    'search' => 'ieškoti',
    'rights' => 'Visos teisės saugomos',
    'department' => [
        'title'          => 'Skyriai',
        'fields'         => [
            'id'                => 'ID',
            'name'              => 'Pavadinimas',
            'email'             => 'El. paštas',
            'country'           => 'Šalis',
            'phone'             => 'Telefonas',
            'comment'           => 'Komentaras',
            'created_at'        => 'Sukurta',
            'updated_at'        => 'Atnaujinta',
            'deleted_at'        => 'Pašalinta'
        ],
    ],
    'settings' => [
        'title'          => 'Nustatymai',
        'fields'         => [
            'id'                => 'ID',
            'name'              => 'Pavadinimas',
            'email'             => 'El. paštas',
            'country'           => 'Šalis',
            'phone'             => 'Telefonas',
            'comment'           => 'Komentaras',
            'created_at'        => 'Sukurta',
            'updated_at'        => 'Atnaujinta',
            'deleted_at'        => 'Pašalinta',
        ],
    ],
    'personal' => [
        'title'          => 'Asmeninis',
        'fields'         => [
            'id'                => 'ID',
            'name'              => 'Pavadinimas',
            'details'           => 'Aprašymas',
            'download'          => 'Atsisiųsti',
            'created_at'        => 'Sukurta',
            'updated_at'        => 'Atnaujinta',
            'deleted_at'        => 'Pašalinta',

            'time_from'         => 'Nuo',
            'time_till'         => 'Iki'
        ],
    ],
];
