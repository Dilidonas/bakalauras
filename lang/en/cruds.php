<?php

return [
    'search' => 'search',
    'rights' => 'All rights reserved',
    'department' => [
        'title'          => 'Transparency Departments',
        'title_singular' => 'Transparency Department',
        'fields'         => [
            'id'                 => 'ID',
            'id_helper'          => '',
            'name'               => 'Name',
            'name_helper'        => '',
            'email'              => 'Email',
            'email_helper'       => '',
            'country'            => 'Country',
            'country_helper'     => '',
            'phone'              => 'Phone',
            'phone_helper'       => '',
            'comment'            => 'Comment',
            'comment_helper'     => '',
            'created_at'        => 'Created',
            'created_at_helper' => '',
            'updated_at'        => 'Updated',
            'updated_at_helper' => '',
            'deleted_at'        => 'Deleted',
            'deleted_at_helper' => ''
        ],
    ],
    'settings' => [
        'title'          => 'Settings',
        'fields'         => [
            'id'                 => 'ID',
            'id_helper'          => '',
            'name'               => 'Pavadinimas',
            'name_helper'        => '',
            'email'              => 'El. paštas',
            'email_helper'       => '',
            'country'            => 'Šalis',
            'country_helper'     => '',
            'phone'              => 'Telefonas',
            'phone_helper'       => '',
            'comment'            => 'Komentaras',
            'comment_helper'     => '',
            'created_at'        => 'Sukurta',
            'created_at_helper' => '',
            'updated_at'        => 'Atnaujinta',
            'updated_at_helper' => '',
            'deleted_at'        => 'Pašalinta',
            'deleted_at_helper' => ''
        ],
    ],
];
