<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code');
            $table->unsignedInteger('continent_id')->nullable();

            $table->foreign('continent_id')
                ->references('id')
                ->on('continents')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        Artisan::call('db:seed', ['--class' => 'CountrySeeder',]);
    }

    public function down()
    {
        Schema::dropIfExists('countries');
    }
};
