<?php

use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

return new class extends Migration
{
    public function up()
    {
        $roleAdmin = Role::create(['name' => 'admin']);
        $roleEmployee = Role::create(['name' => 'employee']);

        $permissionAdd = Permission::create(['name' => 'add employee']);
        $permissionEdit = Permission::create(['name' => 'edit table']);
        $permissionWork = Permission::create(['name' => 'work']);

        $roleAdmin->givePermissionTo($permissionAdd, $permissionEdit);
        $roleEmployee->givePermissionTo($permissionWork);
    }
};
