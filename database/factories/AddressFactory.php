<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class AddressFactory extends Factory
{
    public function definition(): array
    {
        return [
            'country_id' => rand(1, 246),
            'city' => $this->faker->city(),
            'address_1' => $this->faker->address(),
            'address_2' => $this->faker->address(),
            'zip_code' => $this->faker->postcode()
        ];
    }
}
