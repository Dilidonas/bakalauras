<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class DepartmentFactory extends Factory
{
    public function definition(): array
    {
        return [
            'name' => $this->faker->city() . ' Department',
            'email' => $this->faker->unique()->safeEmail(),
            'country_id' => rand(1, 246),
            'phone' => $this->faker->phoneNumber,
            'comment' => $this->faker->sentence
        ];
    }
}
