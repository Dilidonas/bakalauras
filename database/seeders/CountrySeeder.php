<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountrySeeder extends Seeder
{
    public function run()
    {
        $sql = file_get_contents(database_path('raw/countries.sql'));
        DB::unprepared($sql);
    }
}
