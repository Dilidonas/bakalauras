<?php

namespace Database\Seeders;

use App\Models\Address;
use App\Models\Department;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        Department::factory(100)->create();
        Address::factory(1)->create();
        User::factory(1)->create();
        User::find(1)->assignRole('admin');
    }
}
