<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContinentSeeder extends Seeder
{
    public function run()
    {
        $sql = file_get_contents(database_path('raw/continents.sql'));
        DB::unprepared($sql);
    }
}
