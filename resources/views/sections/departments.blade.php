@extends('layouts.main')

@section('css-scripts')
    <link href="{{ asset('css/departments.css') }}" rel="stylesheet" />
@endsection

@section('content')
    <div style="margin-bottom: 10px;" class="container">
        <span class="tab-name">{{ __('cruds.department.title') }}</span>
        <div id="download-button" class="inlineButton">
            <a class="btn btn-success toggle-button" id="download"><i class="fa-solid fa-arrow-down"></i></a>
        </div>

        <div class="inlineButton" data-bs-toggle="modal" data-bs-target="#addNewModal">
            <a class="btn button toggle-button" id="addRow"><i class="fa-solid fa-plus"></i></a>
        </div>

        <div class="inlineButton">
            <a class="btn button toggle-button" id="openFilter"><i class="fa-solid fa-filter"></i></a>
        </div>

        <div id="remove-button" class="inlineButton" style="display: none">
            <a class="btn button toggle-button" id="removeDepartment" style="background: #c13a3a;">
                <i class="fa-solid fa-xmark" style="color: white"></i>
            </a>
        </div>

        <div class="modal fade" id="addNewModal" tabindex="-1" aria-labelledby="addDepartment" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content" style="border-radius: 15px; width: 100%">
                    <div class="modal-header">
                        <h5 class="modal-title" id="addDepartment">Skyriaus pridėjimas</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="addDepartmentForm" action="{{ route("departments.store") }}">
                            @csrf
                            <div class="row g-3 departmentFields align-items-center">
                                <div class="col-12">
                                    <input type="text" class="form-control" name="name" aria-label="Pavadinimas" placeholder="Pavadinimas">
                                </div>
                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" aria-label="ePastas" placeholder="El. paštas">
                                </div>
                                <div class="col-auto">
                                    <label for="country" class="col-form-label">Šalis:</label>
                                </div>
                                <div class="col">
                                    <select id="countryPick" class="selectpicker" aria-label="Valstybė" data-live-search="true">
                                    @foreach($countries as $country)
                                        <option value="{{$country->id}}">{{$country->name}}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <div class="col-12">
                                    <input type="tel" class="form-control" name="phone" aria-label="Telefonas" placeholder="Telefonas">
                                </div>
                                <div class="addComment">
                                    <button id="newComment" type="button" class="new-button">
                                        <i class="fa fa-plus" style="color: #45484b; margin-top: 6px"></i>
                                    </button>
                                    <span>Pridėti komentarą</span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="submitDepartment" form='addDepartmentForm' type="submit" class="btn btn-primary">Išsaugoti</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="sLetters">
            <div class='clear-selection letter'><i class="fa-solid fa-xmark"></i></div>
            <div class="nLetter">#</div>
        </div>

        <div style="display: none; margin-top: 20px;" id="filterForm">
            <div class="row">
                <div class="col-4">
                    <label for="filterContinent" class="form-label">Žemynas</label>
                    <select id="filterContinent" class='selectpicker' data-live-search="true">
                        <option value="0">Visi</option>
                        @foreach($continents as $continent)
                            <option value="{{$continent->id}}">{{$continent->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-4">
                    <label for="filterCountry" class="form-label">Šalis</label>
                    <select id="filterCountry" class='selectpicker' data-live-search="true">
                        <option value="0">Visos</option>
                        @foreach($countries as $country)
                            <option value="{{$country->id}}">{{$country->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-4">
                    <div class="form-check" style="margin-top: 9px;">
                        <input class="form-check-input" type="checkbox" id="mailSent">
                        <label class="form-check-label" for="mailSent">Išsiųstas laiškas</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="withComment">
                        <label class="form-check-label" for="withComment">Su komentaru</label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="restriction"></div>

    <div class="container" style="margin-top: 4%">
        <table class="table row-border table-hover" id="datatable-Department" style="width:100%">
            <thead>
                <tr>
                    <th style="width: 20px"><label class="checkboxColor"><input type="checkbox" id="selectAll"/></label></th>
                    <th>{{ __('cruds.department.fields.name') }}</th>
                    <th>{{ __('cruds.department.fields.email') }}</th>
                    <th>{{ __('cruds.department.fields.country') }}</th>
                    <th>{{ __('cruds.department.fields.phone') }}</th>
                    <th>{{ __('cruds.department.fields.comment') }}</th>
                    <th style="width: 20px"></th>
                </tr>
            </thead><tbody></tbody>
        </table>
    </div>
@endsection

@section('js-scripts')
    <script type="text/javascript" src="{{ asset('js/departments.js') }}"></script>
@endsection
