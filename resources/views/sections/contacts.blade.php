@extends('layouts.main')

@section('css-scripts')
    <link href="{{ asset('css/contacts.css') }}" rel="stylesheet" />
@endsection

@section('content')
    <div style="margin-bottom: 10px;" class="container">
        <span class="tab-name">Kontaktai</span>

        <div class="inlineButton">
            <a class="btn button toggle-button" id="openFilter"><i class="fa-solid fa-filter"></i></a>
        </div>

        <div id="remove-button" class="inlineButton" style="display: none">
            <a class="btn button toggle-button" id="removeContact" style="background: #c13a3a;">
                <i class="fa-solid fa-xmark" style="color: white"></i>
            </a>
        </div>

        <div id="sLetters">
            <div class='clear-selection letter'><i class="fa-solid fa-xmark"></i></div>
            <div class="nLetter">#</div>
        </div>

        <div style="display: none; margin-top: 20px;" id="filterForm">
            <div class="row">
                <div class="col-4">
                    <label for="filterContinent" class="form-label">Žemynas</label>
                    <select id="filterContinent" class='selectpicker' data-live-search="true">
                        <option value="0">Visi</option>
                        @foreach($continents as $continent)
                            <option value="{{$continent->id}}">{{$continent->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-4">
                    <label for="filterCountry" class="form-label">Šalis</label>
                    <select id="filterCountry" class='selectpicker' data-live-search="true">
                        <option value="0">Visos</option>
                        @foreach($countries as $country)
                            <option value="{{$country->id}}">{{$country->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-4">
                    <label for="filterPosition" class="form-label">Pareigos</label>
                    <select id="filterPosition" class='selectpicker' data-live-search="true">
                        <option value="0">Visos</option>
                        @foreach($positions as $position)
                            <option value="{{$position->id}}">{{$position->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div id="restriction"></div>

    <div class="container" style="margin-top: 4%">
        <table class="table row-border table-hover" id="datatable-Contact" style="width:100%">
            <thead>
                <tr>
                    <th style="width: 20px"><label class="checkboxColor"><input type="checkbox" id="selectAll"/></label></th>
                    <th style="width: 10px"></th>
                    <th>Vardas</th>
                    <th>Pavardė</th>
                    <th>El. paštas</th>
                    <th>Valstybė</th>
                    <th>Pareigos</th>
                    <th>Skyrius</th>
                    <th>Telefonas</th>
                    <th>Įmonėje nuo</th>
                    <th style="width: 20px"></th>
                </tr>
            </thead><tbody></tbody>
        </table>
    </div>
@endsection

@section('js-scripts')
    <script type="text/javascript" src="{{ asset('js/contacts.js') }}"></script>
@endsection
