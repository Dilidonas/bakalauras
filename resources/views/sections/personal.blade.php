@extends('layouts.main')

@section('css-scripts')
    <link href="https://cdn.jsdelivr.net/npm/@eonasdan/tempus-dominus@6.0.0-beta4/dist/css/tempus-dominus.min.css" rel="stylesheet" crossorigin="anonymous">
    <link href="{{ asset('css/personal.css') }}" rel="stylesheet"/>
@endsection

@section('content')
    <div style="margin-bottom: 20px; padding: 0" class="container">
        <span class="tab-name">{{ __('cruds.personal.title') }}</span>

        <div id="remove-button" class="inlineButton" style="display: none">
            <a class="btn button toggle-button" id="removeFile" style="background: #c13a3a;">
                <i class="fa-solid fa-xmark" style="color: white"></i>
            </a>
        </div>
    </div>

    <div class="container files" style="padding: 0; float: left; width: 60%;">
        <div id="add-file">
            <a class="btn button toggle-button" id="addFile"><i class="fa-solid fa-plus"></i></a>
            <span style="letter-spacing: 0.5px;">Pridėti failą</span>
        </div>

        <div style="display: none; width: 90%" id="fileToggle">
            <form id="addFileForm" action="{{ route("personal.store") }}">
                @csrf
                <div class="row" style="margin-top: 15px;">
                    <div class="col-7">
                        <input class="form-control" type="text" placeholder="Pavadinimas" aria-label="Pavadinimas"
                               name="title">
                    </div>
                    <div class="col-5">
                        <input class="form-control form-control" id="formFile" type="file" name="file">
                    </div>
                    <div class="mb-3" style="margin-top: 15px;">
                        <textarea class="form-control" aria-label="Aprašymas" name="details"></textarea>
                    </div>
                    <div class="col-12" style="display: flex; align-items: center">
                        <button id="submitFile" type="submit" class="btn btn-success round">Įkelti</button>
                        <span class="error-message"></span>
                    </div>
                </div>
            </form>
        </div>

        <table class="col table row-border table-hover" id="datatable-PersonalFiles" style=" margin: 0">
            <thead>
            <tr>
                <th style="width: 20px"><label class="checkboxColor"><input type="checkbox" id="selectAll"/></label>
                </th>
                <th>{{ __('cruds.personal.fields.name') }}</th>
                <th>{{ __('cruds.personal.fields.details') }}</th>
                <th>{{ __('cruds.personal.fields.created_at') }}</th>
                <th style="width: 20px"></th>
                <th style="width: 20px"></th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>

    <div class="container" style="padding: 0; float: right; width: 35%;">
        <form id="addWorkTimeForm" action="{{ route("work-time") }}">
            @csrf
            <div class="row">
                <div class="col-2">
                    <a class="btn button toggle-button" id="addTime"><i class="fa-solid fa-plus"></i></a>
                </div>
                <div class="col-5">
                    <input type="text" class="form-control datetimepicker-input" id="date-from" data-toggle="date-from" data-target="#date-from" name="date_from"/>
                </div>
                <div class="col-5">
                    <input type="text" class="form-control datetimepicker-input" id="date-till" data-toggle="date-till" data-target="#date-till" name="date_till"/>
                </div>
            </div>
        </form>

        <table class="col table row-border table-hover" id="datatable-WorkTime" style=" margin: 0">
            <thead>
            <tr>
                <th>Diena</th>
                <th>{{ __('cruds.personal.fields.time_from') }}</th>
                <th>{{ __('cruds.personal.fields.time_till') }}</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
@endsection

@section('js-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.11.5/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@eonasdan/tempus-dominus@6.0.0-beta4/dist/js/tempus-dominus.min.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('js/personal.js') }}"></script>
@endsection
