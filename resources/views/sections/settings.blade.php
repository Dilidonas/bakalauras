@extends('layouts.main')

@section('css-scripts')
    <link href="{{ asset('css/settings.css') }}" rel="stylesheet"/>
@endsection

@section('content')
    <div style="margin-bottom: 10px;" class="container">
        <span class="tab-name">{{ __('cruds.settings.title') }}</span>
    </div>

    <div class="container">
        <div class="buttons-side" style="width: 30%; float: left">
            <div id="edit-personal">
                <a class="btn button toggle-button" id="edit-personal"><i class="fa-solid fa-plus"></i></a>
                <span>Redaguoti asmeninius duomenis</span>
            </div>

            <div id="add-employee" style="margin-top: 10px;">
                <a class="btn button toggle-button" id="addRow"><i class="fa-solid fa-plus"></i></a>
                <span>Pridėti darbuotoją</span>
            </div>

            <div id="add-position" style="margin-top: 10px;">
                <a class="btn button toggle-button" id="add-position"><i class="fa-solid fa-plus"></i></a>
                <span>Pridėti pareigas</span>
            </div>
        </div>

        <div class="toggle-side" style="width: 70%; float: right">
            <div id="personal-card" class="card employee-card">
                <div class="card-body">
                    <form id="editPersonalForm" class="row g-3" data-id="{{ $personal->id }}">
                        @csrf
                        <div class="col-md-6">
                            <label for="inputName" class="form-label">Vardas</label>
                            <input type="text" class="form-control" id="inputName" name="name"
                                   value="{{ $personal->name }}">
                        </div>
                        <div class="col-md-6">
                            <label for="inputSurname" class="form-label">Pavardė</label>
                            <input type="text" class="form-control" id="inputSurname" name="surname"
                                   value="{{ $personal->surname }}">
                        </div>
                        <div class="col-md-6">
                            <label for="inputEmail" class="form-label">El. paštas</label>
                            <input type="email" class="form-control" id="inputEmail" name="email"
                                   value="{{ $personal->email }}">
                        </div>
                        <div class="col-md-6">
                            <label for="inputPhone" class="form-label">Telefonas</label>
                            <input type="tel" class="form-control" id="inputPhone" name="phone"
                                   value="{{ $personal->phone }}">
                        </div>
                        <div class="col-6">
                            <label for="inputAddress1" class="form-label">Adresas (1)</label>
                            <input type="text" class="form-control" id="inputAddress1"
                                   name="address_1" value="{{ $personal->address->address_1 }}">
                        </div>
                        <div class="col-6">
                            <label for="inputAddress2" class="form-label">Adresas (2)</label>
                            <input type="text" class="form-control" id="inputAddress2" name="address_2"
                                   value="{{ $personal->address->address_2 }}">
                        </div>
                        <div class="col-md-6">
                            <label for="inputCity" class="form-label">Miestas</label>
                            <input type="text" class="form-control" id="inputCity" name="city"
                                   value="{{ $personal->address->city }}">
                        </div>
                        <div class="col-md-6">
                            <label for="inputZip" class="form-label">Pašto kodas</label>
                            <input type="text" class="form-control" id="inputZip" name="zip_code"
                                   value="{{ $personal->address->zip_code }}">
                        </div>
                        <div class="col-12">
                            <button id="submitEditPersonal" type="submit" class="btn btn-success round">Keisti</button>
                        </div>
                    </form>
                </div>
            </div>

            <div style="display: none; width: 90%" class="position-card">
                <form id="addPositionForm">
                    @csrf
                    <div class="row" style="margin-top: 15px;">
                        <div class="col-6">
                            <input class="form-control" type="text" placeholder="Pavadinimas" aria-label="Pavadinimas"
                                   name="name">
                        </div>
                        <div class="col-12" style="display: flex; align-items: center; margin-top: 10px;">
                            <button id="submitPosition" type="submit" class="btn btn-success round">Pridėti</button>
                            <span class="error-message"></span>
                        </div>
                    </div>
                </form>
            </div>

            <div id="employee-card" class="card employee-card">
                <div class="card-body">
                    <form id="newEmployeeForm" class="row g-3">
                        @csrf
                        <div class="col-md-6">
                            <label for="inputName" class="form-label">Vardas</label>
                            <input type="text" class="form-control" id="inputName" name="name">
                        </div>
                        <div class="col-md-6">
                            <label for="inputSurname" class="form-label">Pavardė</label>
                            <input type="text" class="form-control" id="inputSurname" name="surname">
                        </div>
                        <div class="col-md-6">
                            <label for="inputDepartment" class="form-label">Skyrius</label>
                            <select id="inputDepartment" class="form-select">
                                @foreach($departments as $department)
                                    <option value="{{$department->id}}">{{$department->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="inputPosition" class="form-label">Pareigos</label>
                            <select id="inputPosition" class="form-select">
                                @foreach($positions as $position)
                                    <option value="{{$position->id}}">{{$position->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="inputEmail" class="form-label">El. paštas</label>
                            <input type="email" class="form-control" id="inputEmail" name="email">
                        </div>
                        <div class="col-md-6">
                            <label for="inputPhone" class="form-label">Telefonas</label>
                            <input type="tel" class="form-control" id="inputPhone" name="phone">
                        </div>
                        <div class="col-6">
                            <label for="inputAddress1" class="form-label">Adresas (1)</label>
                            <input type="text" class="form-control" id="inputAddress1" placeholder="Gedimino pr. 99"
                                   name="address_1">
                        </div>
                        <div class="col-6">
                            <label for="inputAddress2" class="form-label">Adresas (2)</label>
                            <input type="text" class="form-control" id="inputAddress2" name="address_2">
                        </div>
                        <div class="col-md-4">
                            <label for="inputCountry" class="form-label">Tautybė</label>
                            <select id="inputCountry" class="form-select">
                                @foreach($countries as $country)
                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="inputCity" class="form-label">Miestas</label>
                            <input type="text" class="form-control" id="inputCity" name="city">
                        </div>
                        <div class="col-md-4">
                            <label for="inputZip" class="form-label">Pašto kodas</label>
                            <input type="text" class="form-control" id="inputZip" name="zip_code">
                        </div>
                        <div class="col-md-6">
                            <label for="inputPassword" class="form-label">Laikinas slaptažodis</label>
                            <input type="password" class="form-control" id="inputPassword" name="password"
                                   onkeyUp="document.getElementById('printPasswordBox').innerHTML = this.value">
                        </div>
                        <div class="col-md-6">
                            <label for="inputPassword" class="form-label">Pakartokite slaptažodį</label>
                            <input type="password" class="form-control" id="repeatPassword"
                                   name="password_confirmation">
                        </div>
                        <div id='printPasswordBox' class='passwordTyping'></div>
                        <div class="col-12">
                            <button id="submitEmployee" type="submit" class="btn btn-primary">Pridėti</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js-scripts')
    <script type="text/javascript" src="{{ asset('js/settings.js') }}"></script>
@endsection
