<header>
    <div class="navbar-section">
        <a href="{{ url('personal') }}" class="{{ request()->is('personal') ? 'active' : '' }}">
            <i class="fa-solid fa-folder-open"></i>
        </a>
        <a href="{{ url('contacts') }}" class="{{ request()->is('contacts') ? 'active' : '' }}">
            <i class="fa-solid fa-address-book"></i>
        </a>
        <a href="{{ url('departments') }}" class="{{ request()->is('departments') ? 'active' : '' }}">
            <i class="fa-solid fa-building"></i>
        </a>
        <a href="{{ url('settings') }}" class="{{ request()->is('settings') ? 'active' : '' }}">
            <i class="fa-solid fa-gear"></i>
        </a>
        @if(!request()->is('personal') && !request()->is('settings'))
        <input type="text" class="search" placeholder="{{ __('cruds.search') }}">
        @endif
    </div>
    <div>
        @if(count(config('panel.available_languages', [])) > 1)
            <div class="btn-group">
                <a data-bs-toggle="dropdown" href="#" class="dropdown-toggle dropdown-toggle-split language-select">
                    <span style="padding-right: 2px; font-weight: 600; color: #77777b">{{ strtoupper(app()->getLocale()) }}</span>
                    <span id="lang-border"></span>
                    <i class="arrow down"></i>
                </a>
                <ul class="dropdown-menu">
                    @foreach(config('panel.available_languages') as $langLocale => $langName)
                        <li><a class="dropdown-item" href="lang/{{ $langLocale }}">{{ strtoupper($langLocale) }} ({{ $langName }})</a></li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form method="POST" action="{{ route('logout') }}" style="display: inline-flex;">
            @csrf
            <button type="submit" id="logout" style="color: #716f6f; border: none">
                <i class="fa-solid fa-arrow-right-from-bracket"></i>
            </button>
        </form>
    </div>
</header>
